import { constants } from "./constants";
import { settings } from "./settings";

export function testJsFiles(buildFolder: string): string {
  return `${buildFolder}/${settings.testSrc}/${constants.allJs}`;
}

export function allTargetFiles(buildFolder: string): string {
  return `${buildFolder}/${constants.allFiles}`;
}
