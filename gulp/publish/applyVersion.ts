import { getGitTagVersion, getNpmVersion, versionHigher, applyNpmVersion } from "./versions";

export async function applyVersion(): Promise<void> {
  // this task is supposed to be executed after tag is set to
  // version to be published
  const version = await getGitTagVersion();
  const npmVersion = await getNpmVersion();
  if (versionHigher(version, npmVersion)) {
    await applyNpmVersion(version);
  }
}
