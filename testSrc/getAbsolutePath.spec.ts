
import { AbsPath } from "../src/parseTypes/createExportModel/models/importedTypes/absPath.type";
import { expect } from "chai";
import { getAbsoluteFolderPath } from "../src/parseTypes/createExportModel/models/importedTypes/getAbsolutePath";

describe("getAbsoluteFolderPath", function (): void {
  it("should return same directory index", async function (): Promise<void> {
    // arrange
    const fileFolder = "c:/someDir/dir2";
    const importPath = ".";
    const expected: AbsPath = { folder: "c:/someDir/dir2", file: "index" };

    // act
    const result = getAbsoluteFolderPath(importPath, fileFolder, null);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should return same directory index when import is slashed", async function (): Promise<void> {
    // arrange
    const fileFolder = "c:/someDir/dir2";
    const importPath = "./";
    const expected: AbsPath = { folder: "c:/someDir/dir2", file: "index" };

    // act
    const result = getAbsoluteFolderPath(importPath, fileFolder, null);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should return same directory from root", async function (): Promise<void> {
    // arrange
    const fileFolder = "c:/";
    const importPath = "./";
    const expected: AbsPath = { folder: "c:", file: "index" };

    // act
    const result = getAbsoluteFolderPath(importPath, fileFolder, null);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should return neighbor file", async function (): Promise<void> {
    // arrange
    const fileFolder = "c:/parent/dir";
    const importPath = "./child";
    const expected: AbsPath = { folder: "c:/parent/dir", file: "child" };

    // act
    const result = getAbsoluteFolderPath(importPath, fileFolder, null);

    // assert
    expect(result).deep.equals(expected);
  });


  it("should return file from parent directory", async function (): Promise<void> {
    // arrange
    const fileFolder = "c:/parent/dir";
    const importPath = "../neighbor";
    const expected: AbsPath = { folder: "c:/parent", file: "neighbor" };

    // act
    const result = getAbsoluteFolderPath(importPath, fileFolder, null);

    // assert
    expect(result).deep.equals(expected);
  });


  it("should return file from neighbor directory", async function (): Promise<void> {
    // arrange
    const fileFolder = "c:/parent/dir";
    const importPath = "../neighbor/file";
    const expected: AbsPath = { folder: "c:/parent/neighbor", file: "file" };

    // act
    const result = getAbsoluteFolderPath(importPath, fileFolder, null);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should not go outside root", async function (): Promise<void> {
    // arrange
    const fileFolder = "c:/parent";
    const importPath = "../../otherPath";
    const expected: AbsPath = { folder: "c:", file: "otherPath" };

    // act
    const result = getAbsoluteFolderPath(importPath, fileFolder, null);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should return aliased path", async function (): Promise<void> {
    // arrange
    const fileFolder = "c:/parent/dir";
    const importPath = "@src/file";
    const expected: AbsPath = { folder: "c:/src", file: "file" };
    const aliasMap = {
      "@src": "c:/src"
    };

    // act
    const result = getAbsoluteFolderPath(importPath, fileFolder, { aliasMap });

    // assert
    expect(result).deep.equals(expected);
  });

  it("should not alter non-aliased path", async function (): Promise<void> {
    // arrange
    const fileFolder = "c:/parent/dir";
    const importPath = "lib";
    const expected: AbsPath = { folder: "", file: "lib" };
    const aliasMap = {
      "@src": "c:/src"
    };

    // act
    const result = getAbsoluteFolderPath(importPath, fileFolder, { aliasMap });

    // assert
    expect(result).deep.equals(expected);
  });

});
