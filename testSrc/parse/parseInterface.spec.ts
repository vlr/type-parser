import { expect } from "chai";
import { parseFile } from "../../src/parseTypes/parseFile/parseFile";
import { ParsedFunction, ParsedInterface } from "../../src/parseTypes/parseFile/types";

describe("parse interface", function (): void {
  it("should parse empty interface", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface {

    }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse empty one-line interface", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface { }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse empty interface with extends", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface extends OtherInterface, ThirdInterface {
    }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: ["OtherInterface", "ThirdInterface"],
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse interface with a field", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface {
    f1: number;
    }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f1",
        typeName: "number",
        comment: null
      }],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse interface with a method", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface {
    f1: (a: number) => string;
    }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: [{
        comment: null,
        returnType: "string",
        name: "f1",
        parameters: [{ parameterName: "a", typeName: "number" }]
      }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse interface with a method and a field", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface {
      f1: (a: number) => string;
      f2: number;
    }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f2",
        typeName: "number",
        comment: null
      }],
      methods: [{
        comment: null,
        returnType: "string",
        name: "f1",
        parameters: [{ parameterName: "a", typeName: "number" }]
      }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse interface with a method and commented field", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface {
      f1: (a: number) => string;
      // field comment
      f2: number;
    }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f2",
        typeName: "number",
        comment: "field comment"
      }],
      methods: [{
        comment: null,
        returnType: "string",
        name: "f1",
        parameters: [{ parameterName: "a", typeName: "number" }]
      }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse interface with commented method and a field", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface {
      // method comment
      f1: (a: number) => string;

      f2: number;
    }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f2",
        typeName: "number",
        comment: null
      }],
      methods: [{
        comment: "method comment",
        returnType: "string",
        name: "f1",
        parameters: [{ parameterName: "a", typeName: "number" }]
      }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse interface with two fields", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface {
      f1: MyType;
      f2: OtherType;
    }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f1",
        typeName: "MyType",
        comment: null
      }, {
        fieldName: "f2",
        typeName: "OtherType",
        comment: null
      }],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse interface with two methods", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface {
      f1: () => MyType;
      f2: (a: Type) =>OtherType;
    }
`;
    const expected: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: [{
        name: "f1",
        parameters: [],
        comment: null,
        returnType: "MyType"
      }, {
        name: "f2",
        parameters: [{ parameterName: "a", typeName: "Type" }],
        comment: null,
        returnType: "OtherType"
      }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expected);
  });

  it("should parse interface and a free function", async function (): Promise<void> {
    // arrange

    const content = `
    export interface SomeInterface {
    }

    export function myFunction(): void { }
`;
    const expectedInterfaces: ParsedInterface[] = [{
      comment: null,
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: []
    }];

    const expectedFunctions: ParsedFunction[] = [{
      name: "myFunction",
      comment: null,
      parameters: [],
      returnType: "void"
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.interfaces).deep.equals(expectedInterfaces);
    expect(result.functions).deep.equals(expectedFunctions);
  });
});
