import { expect } from "chai";
import { parseFile } from "../../src/parseTypes/parseFile/parseFile";
import { ParsedClass } from "../../src/parseTypes/parseFile/types";

describe("parse class", function (): void {
  it("should parse empty class", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass {

    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: null,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });

  it("should parse empty one-line class", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass { }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: null,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });

  it("should parse class with extends", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass extends OtherClass {
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: ["OtherClass"],
      implements: [],
      constructor: null,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });

  it("should parse class with implements", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass implements SomeInterface, OtherInterface {
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: ["SomeInterface", "OtherInterface"],
      constructor: null,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });

  it("should parse class with extends and implements", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass extends OtherClass implements SomeInterface, OtherInterface {
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: ["OtherClass"],
      implements: ["SomeInterface", "OtherInterface"],
      constructor: null,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });

  it("should parse class with one parameter constructor", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass {
      constructor(a: number){
      }
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comment: null,
        parameters: [{
          parameterName: "a",
          typeName: "number"
        }],
      },
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });

  it("should parse class with constructor and public parameter", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass {
      constructor(public a: number){
      }
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comment: null,
        parameters: [{
          parameterName: "a",
          typeName: "number"
        }]
      },
      fields: [{
        fieldName: "a",
        typeName: "number",
        comment: null
      }],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });

  it("should parse class with constructor and public/private parameters", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass {
      constructor(private b: string, public a: number){
      }
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comment: null,
        parameters: [{
          parameterName: "b",
          typeName: "string"
        }, {
          parameterName: "a",
          typeName: "number"
        }]
      },
      fields: [{
        fieldName: "a",
        typeName: "number",
        comment: null
      }],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });


  it("should parse class with method and then constructor ", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass {
      public m1(): void {

      }
      constructor(private b: string){
      }
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comment: null,
        parameters: [{
          parameterName: "b",
          typeName: "string"
        }]
      },
      fields: [],
      methods: [{
        name: "m1",
        parameters: [],
        returnType: "void",
        comment: null
      }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });



  it("should parse class with empty constructor, field and commented method ", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass {
      constructor() {
      }

      public f1: MyType<number, MyOtherType> = 10;

      // method comment
      public m1(): void {
      }
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comment: null,
        parameters: []
      },
      fields: [{
        fieldName: "f1",
        typeName: "MyType<number, MyOtherType>",
        comment: null
      }],
      methods: [{
        name: "m1",
        parameters: [],
        returnType: "void",
        comment: "method comment"
      }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });

  it("should parse class method returning anonymous object ", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass {
      public m1(): {a: string} {
      }
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: null,
      fields: [],
      methods: [{
        name: "m1",
        parameters: [],
        returnType: "{a: string}",
        comment: null
      }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });


  it("should parse class and ignore private fields in it ", async function (): Promise<void> {
    // arrange

    const content = `
    export class SomeClass {
      private m1(): {a: string} {
      }

      private f1: string;

      public f2: number;
    }
`;
    const expected: ParsedClass[] = [{
      comment: null,
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: null,
      fields: [{
        fieldName: "f2",
        typeName: "number",
        comment: null
      }],
      methods: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.classes).deep.equals(expected);
  });
});
