import { parseFile } from "../../src/parseTypes/parseFile/parseFile";
import { expect } from "chai";
import { ParsedType } from "../../src/parseTypes/parseFile/types";

describe("parse type", function (): void {
  it("should parse union type", async function (): Promise<void> {
    // arrange

    const content = `
    export type t1 = "a" | "b";
`;
    const expected: ParsedType[] = [{
      comment: null,
      name: "t1"
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.types).deep.equals(expected);
  });

  it("should parse function type, interface type and union", async function (): Promise<void> {
    // arrange

    const content = `
    export type t1 = function(): void { };

    export type t2 = { a: number };

    export type t3 = "a" | "b";
`;
    const expected: ParsedType[] = [{
      comment: null,
      name: "t1"
    }, {
      comment: null,
      name: "t2"
    }, {
      comment: null,
      name: "t3"
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.types).deep.equals(expected);
  });

  it("should parse constant and type with comment", async function (): Promise<void> {
    // arrange

    const content = `
    // type comment
    export type t1 = function(): void { };

    export constant c2 = { a: number };
`;
    const expectedTypes: ParsedType[] = [{
      comment: "type comment",
      name: "t1"
    }];

    const expectedConstants: ParsedType[] = [{
      comment: null,
      name: "c2"
    }];
    // act
    const result = parseFile(content);

    // assert
    expect(result.types).deep.equals(expectedTypes);
    expect(result.constants).deep.equals(expectedConstants);
  });
});
