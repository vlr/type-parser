import { parseFile } from "../../src/parseTypes/parseFile/parseFile";
import { expect } from "chai";
import { ParsedEnum } from "../../src/parseTypes/parseFile/types";

describe("parse enum", function (): void {
  it("should parse empty enum", async function (): Promise<void> {
    // arrange

    const content = `
    export enum MyEnum {}
`;
    const expected: ParsedEnum[] = [{
      comment: null,
      name: "MyEnum",
      values: []
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.enums).deep.equals(expected);
  });

  it("should parse enum with values and comments", async function (): Promise<void> {
    // arrange

    const content = `
    // enum comment
    export enum MyEnum {
      a = 1,
      // value comment
      b = "v",
      c
    }
`;
    const expected: ParsedEnum[] = [{
      comment: "enum comment",
      name: "MyEnum",
      values: [{
        valueName: "a",
        comment: null
      }, {
        valueName: "b",
        comment: "value comment"
      }, {
        valueName: "c",
        comment: null
      }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.enums).deep.equals(expected);
  });

});
