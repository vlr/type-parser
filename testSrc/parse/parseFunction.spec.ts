import { expect } from "chai";
import { parseFile } from "../../src/parseTypes/parseFile/parseFile";
import { ParsedFunction } from "../../src/parseTypes/parseFile/types";

describe("parse functions", function (): void {
  it("should parse a function", async function (): Promise<void> {
    // arrange

    const content = `
    export function someFunction(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comment: null,
      name: "someFunction",
      returnType: "number",
      parameters: [{ parameterName: "par", typeName: "string" }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.functions).deep.equals(expected);
  });

  it("should parse one line function", async function (): Promise<void> {
    // arrange

    const content = `
    export function someFunction(par: string): number { console.log("hello"); }
`;
    const expected: ParsedFunction[] = [{
      comment: null,
      name: "someFunction",
      returnType: "number",
      parameters: [{ parameterName: "par", typeName: "string" }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.functions).deep.equals(expected);
  });

  it("should parse function with comment", async function (): Promise<void> {
    // arrange

    const content = `
    // very good function
    export function someFunction(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comment: "very good function",
      name: "someFunction",
      returnType: "number",
      parameters: [{ parameterName: "par", typeName: "string" }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.functions).deep.equals(expected);
  });

  it("should only include comment if only spaces between function and comment", async function (): Promise<void> {
    // arrange

    const content = `
    // comment is not for a function
    const a = 1;
    export  function someFunction(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comment: null,
      name: "someFunction",
      returnType: "number",
      parameters: [{ parameterName: "par", typeName: "string" }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.functions).deep.equals(expected);
  });

  it("should not include commented function", async function (): Promise<void> {
    // arrange

    const content = `
   // export function someFunction(par: string): number {
   /* export function someFunction(par: string): number {
      console.log("hello");
    }*/
`;
    const expected: ParsedFunction[] = [];

    // act
    const result = parseFile(content);

    // assert
    expect(result.functions).deep.equals(expected);
  });

  it("should parse three functions", async function (): Promise<void> {
    // arrange

    const content = `

    export function someFunction(par: string): number {
      console.log("hello");
    }

    // second
    export function someFunction2(par: string): number {
      console.log("hello");
    }

    export function someFunction3(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comment: null,
      name: "someFunction",
      returnType: "number",
      parameters: [{ parameterName: "par", typeName: "string" }]
    },
    {
      comment: "second",
      name: "someFunction2",
      returnType: "number",
      parameters: [{ parameterName: "par", typeName: "string" }]
    },
    {
      comment: null,
      name: "someFunction3",
      returnType: "number",
      parameters: [{ parameterName: "par", typeName: "string" }]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.functions).deep.equals(expected);
  });

  it("should parse function with several parameters", async function (): Promise<void> {
    // arrange

    const content = `

    export function someFunction(par1: string, par2: number): number {
      console.log("hello");
    }

`;
    const expected: ParsedFunction[] = [{
      comment: null,
      name: "someFunction",
      returnType: "number",
      parameters: [
        { parameterName: "par1", typeName: "string" },
        { parameterName: "par2", typeName: "number" }
      ]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.functions).deep.equals(expected);
  });

  it("should parse function with generic parameters", async function (): Promise<void> {
    // arrange

    const content = `

    export function someFunction(par1: GenType<string, number>, par2: number): number {
      console.log("hello");
    }

`;
    const expected: ParsedFunction[] = [{
      comment: null,
      name: "someFunction",
      returnType: "number",
      parameters: [
        { parameterName: "par1", typeName: "GenType<string, number>" },
        { parameterName: "par2", typeName: "number" }
      ]
    }];

    // act
    const result = parseFile(content);

    // assert
    expect(result.functions).deep.equals(expected);
  });

});
