import { expect } from "chai";
import { parseFile } from "../../src/parseTypes/parseFile/parseFile";
import { ParsedImport } from "../../src/parseTypes/parseFile/types";

describe("parse imports", function (): void {
  it("should parse singular imports", async function (): Promise<void> {
    // arrange

    const content = `
    import { Type1 } from '.';
    import { Type2 } from './';
`;
    const expected: ParsedImport[] = [
      { type: "Type1", alias: "Type1", importPath: "." },
      { type: "Type2", alias: "Type2", importPath: "./" }
    ];

    // act
    const result = parseFile(content);

    // assert
    checkImports(result.imports, expected);
  });

  it("should ignore commented line", async function (): Promise<void> {
    // arrange

    const content = `
    // import { Type1 } from '.';
    import { Type2 } from './';
`;
    const expected: ParsedImport[] = [
      { type: "Type2", alias: "Type2", importPath: "./" }
    ];

    // act
    const result = parseFile(content);

    // assert
    checkImports(result.imports, expected);
  });

  it("should ignore star comments", async function (): Promise<void> {
    // arrange

    const content = `
    /* import { Type1 } from '.';
    */ import { Type2 } from './';
`;
    const expected: ParsedImport[] = [
      { type: "Type2", alias: "Type2", importPath: "./" }
    ];

    // act
    const result = parseFile(content);

    // assert
    checkImports(result.imports, expected);
  });

  it("should parse multi imports", async function (): Promise<void> {
    // arrange
    const content = `
    import { Type1, Type2 } from '.';
`;
    const expected: ParsedImport[] = [
      { type: "Type1", alias: "Type1", importPath: "." },
      { type: "Type2", alias: "Type2", importPath: "." }
    ];

    // act
    const result = parseFile(content);

    // assert
    checkImports(result.imports, expected);
  });

  it("should parse aliased imports", async function (): Promise<void> {
    // arrange
    const content = `
    import { Type1 as Type2, Type3 } from './someDir';
`;
    const expected: ParsedImport[] = [
      { type: "Type1", alias: "Type2", importPath: "./someDir" },
      { type: "Type3", alias: "Type3", importPath: "./someDir" }
    ];

    // act
    const result = parseFile(content);

    // assert
    checkImports(result.imports, expected);
  });

  it("should parse multiline imports", async function (): Promise<void> {
    // arrange
    const content = `
    import { Type1 as Type2,
       Type3 } from './someDir';
    import {
      Type4 as Type5,
      Type6 } from './someDir';
    import {
        Type7 as Type8,
        Type9
    } from './someDir';
`;
    const expected: ParsedImport[] = [
      { type: "Type1", alias: "Type2", importPath: "./someDir" },
      { type: "Type3", alias: "Type3", importPath: "./someDir" },
      { type: "Type4", alias: "Type5", importPath: "./someDir" },
      { type: "Type6", alias: "Type6", importPath: "./someDir" },
      { type: "Type7", alias: "Type8", importPath: "./someDir" },
      { type: "Type9", alias: "Type9", importPath: "./someDir" }
    ];

    // act
    const result = parseFile(content);

    // assert
    checkImports(result.imports, expected);
  });

});

function checkImports(imports: ParsedImport[], expected: ParsedImport[]): void {
  expect(imports).not.equal(undefined);
  expect(imports.length).equal(expected.length);
  for (let exp of expected) {
    const imp = imports.find(i => i.importPath === exp.importPath && i.type === exp.type);
    expect(imp).not.equal(undefined);
    expect(imp.alias).equal(exp.alias);
  }
}
