import { expect } from "chai";
import {
  ParsedModel, ParsedType, ParsedImport, ParsedEnum, ParsedClass,
  ParsedField, ParsedFunction, ParsedInterface
} from "../src/parseTypes/parseFile/types";
import { createExportModel } from "../src/parseTypes/createExportModel/createExportModel";
import { FileToParse } from "../src";
import { TypesModel, TypeModel, EnumModel, MethodModel, ClassModel, FieldModel, InterfaceModel } from "../src/types";

describe("createExportModel", function (): void {
  it("should create export model from parsed", async function (): Promise<void> {
    // arrange
    const file: FileToParse = { name: "f1", folder: "c:/folder", contents: "" };

    const imp: ParsedImport = { type: "a", alias: "a", importPath: "./b" };
    const type: ParsedType = { name: "type1", comment: "typeComment" };
    const en: ParsedEnum = { name: "enum1", comment: null, values: [{ valueName: "val1", comment: null }] };
    const clsMethod: ParsedFunction = { name: "func1", comment: null, parameters: [{ parameterName: "p1", typeName: "string" }], returnType: "void" };
    const cls: ParsedClass = {
      name: "class1", comment: null, fields: [],
      methods: [clsMethod], constructor: { parameters: [], comment: null }, extends: [], implements: []
    };
    const infField: ParsedField = { fieldName: "field1", comment: null, typeName: "a" };
    const inf: ParsedInterface = { name: "iFace1", comment: null, fields: [infField], methods: [], extends: [] };
    const constant: ParsedType = { name: "constant", comment: null };
    const parsed: ParsedModel = {
      imports: [imp],
      types: [type],
      enums: [en],
      classes: [cls],
      interfaces: [inf],
      functions: [],
      constants: [constant]
    };

    const expType: TypeModel = {
      id: { file: "f1", folder: "c:/folder", name: "type1" },
      comment: "typeComment"
    };

    const expEnum: EnumModel = {
      id: { file: "f1", folder: "c:/folder", name: "enum1" },
      comment: null,
      values: [{ valueName: "val1", comment: null }]
    };
    const expClassMethod: MethodModel = {
      name: "func1", comment: null,
      parameters: [{ parameterName: "p1", parameterType: { typeName: "string", importedType: null, importedTypes: [] } }],
      returnType: { typeName: "void", importedType: null, importedTypes: [] }
    };
    const expClass: ClassModel = {
      id: { file: "f1", folder: "c:/folder", name: "class1" },
      fields: [],
      comment: null,
      constructor: { parameters: [], comment: null },
      methods: [expClassMethod],
      extends: [],
      implements: []
    };

    const expIntField: FieldModel = {
      fieldName: "field1",
      comment: null,
      fieldType: {
        typeName: "a", importedType: { alias: "a", id: { file: "b", folder: "c:/folder", name: "a" } },
        importedTypes: [{ alias: "a", id: { file: "b", folder: "c:/folder", name: "a" } }]
      }
    };

    const expInterface: InterfaceModel = {
      id: { file: "f1", folder: "c:/folder", name: "iFace1" },
      comment: null,
      fields: [expIntField],
      methods: [],
      extends: []
    };

    const expConst: TypeModel = {
      id: { file: "f1", folder: "c:/folder", name: "constant" },
      comment: null
    };

    const expected: TypesModel<FileToParse> = {
      file,
      types: [expType],
      enums: [expEnum],
      classes: [expClass],
      interfaces: [expInterface],
      functions: [],
      constants: [expConst]
    };

    // act
    const result = createExportModel(file, parsed, null);

    // assert
    expect(result).deep.equals(expected);
  });

});
