export function combinePath(...parts: string[]): string {
  return parts.filter(x => x.length > 0).join("/").replace(/\/+/g, "/");
}
