import { TypeModel, TypesModel, TypeId } from "../types";

export function exportedTypes(model: TypesModel): string[] {
  return exportedTypeIds(model).map(id => id.name);
}

export function exportedTypeIds(model: TypesModel): TypeId[] {
  const typeName = (type: TypeModel) => type.id;
  return [
    ...model.classes.map(typeName),
    ...model.interfaces.map(typeName),
    ...model.enums.map(typeName),
    ...model.types.map(typeName),
    ...model.functions.map(typeName),
    ...model.constants.map(typeName)
  ];
}
