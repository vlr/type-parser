import { TypeDefinition } from "../types";

export function isMonotype(field: TypeDefinition): boolean {
  return field.importedType != null;
}
