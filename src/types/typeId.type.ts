export interface TypeId {
  folder: string;
  file: string;
  name: string;
}

export interface ImportedType {
  alias: string;
  id: TypeId;
}
