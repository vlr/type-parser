import { TypeModel } from "./type.model";

export interface EnumModel extends TypeModel {
  values: EnumValue[];
}

export interface EnumValue {
  valueName: string;
  comment: string;
}
