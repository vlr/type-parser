import { FieldModel } from "./field.model";
import { ConstructorModel, MethodModel } from "./function.model";
import { TypeModel } from "./type.model";
import { TypeDefinition } from "./typeDefinition.type";

export interface ClassModel extends TypeModel {
  fields: FieldModel[];
  constructor: ConstructorModel;
  methods: MethodModel[];
  extends: TypeDefinition[];
  implements: TypeDefinition[];
}
