import { EnumModel } from "./enumModel.type";
import { InterfaceModel } from "./interfaceModel.type";
import { ClassModel } from "./classModel.type";
import { FunctionModel } from "./function.model";
import { FileToParse } from "../parseTypes";
import { TypeModel } from "./type.model";

export interface TypesModel<T extends FileToParse = FileToParse> {
  file: T;
  types: TypeModel[]; // types exported as "export type A = ...;"
  enums: EnumModel[];
  interfaces: InterfaceModel[];
  classes: ClassModel[];
  functions: FunctionModel[];
  constants: TypeModel[];
}


