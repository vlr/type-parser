import { TypeDefinition } from "./typeDefinition.type";
import { ParameterModel } from "./parameter.model";
import { TypeId } from "./typeId.type";

export interface FunctionModel {
  comment: string;
  id: TypeId;
  returnType: TypeDefinition;
  parameters: ParameterModel[];
}

export interface MethodModel {
  comment: string;
  name: string;
  returnType: TypeDefinition;
  parameters: ParameterModel[];
}

export interface ConstructorModel {
  comment: string;
  parameters: ParameterModel[];
}
