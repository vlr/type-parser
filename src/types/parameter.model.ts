import { TypeDefinition } from "./typeDefinition.type";

export interface ParameterModel {
  parameterName: string;
  parameterType: TypeDefinition;
}
