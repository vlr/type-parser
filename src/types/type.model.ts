import { TypeId } from "./typeId.type";

export interface TypeModel {
  id: TypeId;
  comment: string;
}
