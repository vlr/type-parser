import { ImportedType } from "./typeId.type";

export interface TypeDefinition {
  typeName: string;
  importedType: ImportedType; // will only be populated for monotype
  importedTypes: ImportedType[];
}
