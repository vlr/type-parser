import { TypeDefinition } from "./typeDefinition.type";

export interface FieldModel {
  fieldName: string;
  comment: string;
  fieldType: TypeDefinition;
}
