import { IMap } from "@vlr/map-tools/objectMap";

export interface ParserOptions {
  aliasMap?: IMap;
}
