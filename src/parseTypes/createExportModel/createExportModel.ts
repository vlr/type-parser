import { TypesModel } from "../../types";
import { FileToParse } from "../fileToParse.type";
import { ParsedModel } from "../parseFile/types";
import { ParserOptions } from "../parserOptions.type";
import { ExportModelContext } from "./exportModelContext.type";
import { mapWith } from "@vlr/fp-conversion";
import { createTypeModel } from "./models/createTypeModel";
import { createEnumModel } from "./models/createEnumModel";
import { createInterfaceModel } from "./models/createInterfaceModel";
import { createClassModel } from "./models/createClassModel";
import { createFunctionModel } from "./models/createFunctionModel";

export function createExportModel<T extends FileToParse>(file: T, parsed: ParsedModel, options: ParserOptions): TypesModel<T> {
  const context: ExportModelContext = { parsed, file, options };
  return {
    file,
    constants: mapWith(createTypeModel)(parsed.constants, context),
    types: mapWith(createTypeModel)(parsed.types, context),
    enums: mapWith(createEnumModel)(parsed.enums, context),
    interfaces: mapWith(createInterfaceModel)(parsed.interfaces, context),
    classes: mapWith(createClassModel)(parsed.classes, context),
    functions: mapWith(createFunctionModel)(parsed.functions, context)
  };
}


