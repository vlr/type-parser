import { ParserOptions } from "../parserOptions.type";
import { FileToParse } from "../fileToParse.type";
import { ParsedModel } from "../parseFile/types";

export interface ExportModelContext {
  parsed: ParsedModel;
  options: ParserOptions;
  file: FileToParse;
}
