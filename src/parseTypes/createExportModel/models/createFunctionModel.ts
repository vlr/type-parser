import { ParsedFunction } from "../../parseFile/types";
import { ExportModelContext } from "../exportModelContext.type";
import { FunctionModel } from "../../../types";
import { createParameterModel } from "./parts/createParameterModel";
import { mapWith } from "@vlr/fp-conversion";
import { createTypeDefinition } from "./parts/createTypeDefinition";
import { createTypeId } from "./importedTypes/createTypeId";

export function createFunctionModel(parsed: ParsedFunction, context: ExportModelContext): FunctionModel {
  return {
    id: createTypeId(parsed.name, context),
    comment: parsed.comment,
    parameters: mapWith(createParameterModel)(parsed.parameters, context),
    returnType: createTypeDefinition(parsed.returnType, context)
  };
}
