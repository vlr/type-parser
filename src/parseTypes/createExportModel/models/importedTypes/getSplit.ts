export function getSplit(path: string): string[] {
  return path.replace(/\\/g, "/").split("/").filter(x => x.length > 0);
}
