export interface AbsPath {
  folder: string;
  file: string;
}
