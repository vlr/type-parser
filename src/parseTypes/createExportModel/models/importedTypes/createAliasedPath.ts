import { IMap } from "@vlr/map-tools/objectMap";
import { AbsPath } from "./absPath.type";
import { createPathFromSplit } from "./createPathFromSplit";

export function createAliasedPath(split: string[], aliasMap: IMap): AbsPath {
  split[0] = aliasMap[split[0]];
  return createPathFromSplit(split);
}
