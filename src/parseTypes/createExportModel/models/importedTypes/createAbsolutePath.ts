import { createPathFromSplit } from "./createPathFromSplit";
import { AbsPath } from "./absPath.type";
import { getSplit } from "./getSplit";
import { trimOne } from "./trimOne";
import { last } from "@vlr/array-tools";

export function createAbsolutePath(split: string[], fileFolder: string): AbsPath {
  let splitFolder = getSplit(fileFolder);
  let index = 0;
  while (index < split.length) {
    const token = split[index];
    switch (token) {
      case ".": break;
      case "..":
        if (splitFolder.length > 1) {
          splitFolder = trimOne(splitFolder);
        }
        break;
      default:
        splitFolder = [...splitFolder, token];
    }
    index++;
  }
  if (last(split) === "." || last(split) === "..") {
    return createPathFromSplit([...splitFolder, "index"]);
  }
  return createPathFromSplit(splitFolder);
}
