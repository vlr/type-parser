import { last } from "@vlr/array-tools";
import { combinePath } from "../../../../functions/combinePath";
import { AbsPath } from "./absPath.type";
import { trimOne } from "./trimOne";

export function createPathFromSplit(split: string[]): AbsPath {
  return {
    file: last(split),
    folder: combinePath(...trimOne(split))
  };
}
