import { memoized } from "@vlr/cache";
import { ImportedType } from "../../../../types";
import { ExportModelContext } from "../../exportModelContext.type";
import { createImportedTypes } from "./createImportedType";

export const getImportedTypes = memoized(function (type: string, context: ExportModelContext): ImportedType[] {
  var types = type.split(/\[|\]|<|>/).filter(x => x.length > 0);
  return createImportedTypes(types, context).filter(x => x != null);
});

export function getMonotypeId(type: string, context: ExportModelContext): ImportedType {
  const types = getImportedTypes(type, context);
  if (types.length !== 1) { return null; }
  if (types[0].alias !== type) { return null; }
  return types[0];
}
