import { memoized } from "@vlr/cache";
import { ExportModelContext } from "../../exportModelContext.type";
import { has } from "@vlr/map-tools/objectMap";
import { ParserOptions } from "../../../parserOptions.type";
import { createPathFromSplit } from "./createPathFromSplit";
import { createAliasedPath } from "./createAliasedPath";
import { createAbsolutePath } from "./createAbsolutePath";
import { getSplit } from "./getSplit";
import { AbsPath } from "./absPath.type";

export function getAbsolutePath(importPath: string, context: ExportModelContext): AbsPath {
  return getAbsoluteFolderPath(importPath, context.file.folder, context.options);
}

export const getAbsoluteFolderPath = memoized(function (importPath: string, fileFolder: string, options: ParserOptions): AbsPath {
  const split = getSplit(importPath);
  if (split.length === 0) { return null; }

  if (split[0] === "." || split[0] === "..") {
    return createAbsolutePath(split, fileFolder);
  }

  if (has(options.aliasMap, split[0])) {
    return createAliasedPath(split, options.aliasMap);
  }

  return createPathFromSplit(split);
});

