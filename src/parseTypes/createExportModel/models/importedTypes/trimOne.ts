export function trimOne<T>(arr: T[]): T[] {
  return arr.slice(0, arr.length - 1);
}
