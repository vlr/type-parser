import { memoized } from "@vlr/cache";
import { toMap } from "@vlr/map-tools";
import { ImportedType } from "../../../../types";
import { ParsedImport, ParsedType } from "../../../parseFile/types";
import { ExportModelContext } from "../../exportModelContext.type";
import { createTypeId } from "./createTypeId";
import { getAbsolutePath } from "./getAbsolutePath";
import { mapWith } from "@vlr/fp-conversion";

export const createImportedType = memoized(function (alias: string, context: ExportModelContext): ImportedType {
  if (exportedTypes(context).has(alias)) {
    const typeId = createTypeId(alias, context);
    return { id: typeId, alias };
  }
  const imp = importsMap(context).get(alias);
  if (imp == null) { return null; }

  const path = getAbsolutePath(imp.importPath, context);
  if (path == null) { return null; }
  return {
    alias: imp.alias,
    id: {
      folder: path.folder,
      file: path.file,
      name: imp.type
    }
  };
});

export const createImportedTypes = mapWith(createImportedType);

const importsMap = memoized(function (context: ExportModelContext): Map<string, ParsedImport> {
  return toMap(context.parsed.imports, imp => imp.alias);
});

const exportedTypes = memoized(function (context: ExportModelContext): Set<string> {
  const model = context.parsed;
  const typeName = (type: ParsedType) => type.name;
  return new Set([
    ...model.classes.map(typeName),
    ...model.interfaces.map(typeName),
    ...model.enums.map(typeName),
    ...model.types.map(typeName),
    ...model.functions.map(typeName),
    ...model.constants.map(typeName)
  ]);
});
