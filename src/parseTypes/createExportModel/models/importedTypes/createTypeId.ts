import { TypeId } from "../../../../types";
import { ExportModelContext } from "../../exportModelContext.type";

export function createTypeId(typeName: string, context: ExportModelContext): TypeId {
  return {
    name: typeName,
    file: context.file.name,
    folder: context.file.folder
  };
}
