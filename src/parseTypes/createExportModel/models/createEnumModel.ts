import { EnumModel } from "../../../types";
import { ParsedEnum } from "../../parseFile/types";
import { createTypeId } from "./importedTypes/createTypeId";
import { ExportModelContext } from "../exportModelContext.type";

export function createEnumModel(parsed: ParsedEnum, context: ExportModelContext): EnumModel {
  return {
    id: createTypeId(parsed.name, context),
    comment: parsed.comment,
    values: parsed.values // maybe will need to map later, so far they are the same
  };
}
