import { mapWith } from "@vlr/fp-conversion";
import { ClassModel, ConstructorModel } from "../../../types";
import { ParsedClass } from "../../parseFile/types";
import { ParsedConstructor } from "../../parseFile/types/parsedConstructor";
import { ExportModelContext } from "../exportModelContext.type";
import { createMethodModel } from "./parts/createMethodModel";
import { createTypeId } from "./importedTypes/createTypeId";
import { createField } from "./parts/createFieldModel";
import { createParameterModel } from "./parts/createParameterModel";
import { createTypeDefinitions } from "./parts/createTypeDefinition";

export function createClassModel(cls: ParsedClass, context: ExportModelContext): ClassModel {
  return {
    id: createTypeId(cls.name, context),
    comment: cls.comment,
    fields: mapWith(createField)(cls.fields, context),
    methods: mapWith(createMethodModel)(cls.methods, context),
    constructor: createConstructor(cls.constructor, context),
    extends: createTypeDefinitions(cls.extends, context),
    implements: createTypeDefinitions(cls.implements, context)
  };
}

function createConstructor(parsed: ParsedConstructor, context: ExportModelContext): ConstructorModel {
  return {
    comment: parsed.comment,
    parameters: mapWith(createParameterModel)(parsed.parameters, context)
  };
}


