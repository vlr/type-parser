import { mapWith } from "@vlr/fp-conversion";
import { InterfaceModel } from "../../../types";
import { ParsedInterface } from "../../parseFile/types";
import { ExportModelContext } from "../exportModelContext.type";
import { createTypeId } from "./importedTypes/createTypeId";
import { createField } from "./parts/createFieldModel";
import { createMethodModel } from "./parts/createMethodModel";
import { createTypeDefinitions } from "./parts/createTypeDefinition";

export function createInterfaceModel(parsed: ParsedInterface, context: ExportModelContext): InterfaceModel {
  return {
    id: createTypeId(parsed.name, context),
    comment: parsed.comment,
    fields: mapWith(createField)(parsed.fields, context),
    methods: mapWith(createMethodModel)(parsed.methods, context),
    extends: createTypeDefinitions(parsed.extends, context)
  };
}
