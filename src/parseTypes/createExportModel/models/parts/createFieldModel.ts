import { FieldModel } from "../../../../types";
import { ParsedField } from "../../../parseFile/types";
import { ExportModelContext } from "../../exportModelContext.type";
import { createTypeDefinition } from "./createTypeDefinition";

export function createField(field: ParsedField, context: ExportModelContext): FieldModel {
  return {
    fieldName: field.fieldName,
    comment: field.comment,
    fieldType: createTypeDefinition(field.typeName, context)
  };
}
