import { ParameterModel } from "../../../../types";
import { ParsedParameter } from "../../../parseFile/types";
import { ExportModelContext } from "../../exportModelContext.type";
import { createTypeDefinition } from "./createTypeDefinition";

export function createParameterModel(parameter: ParsedParameter, context: ExportModelContext): ParameterModel {
  return {
    parameterName: parameter.parameterName,
    parameterType: createTypeDefinition(parameter.typeName, context)
  };
}
