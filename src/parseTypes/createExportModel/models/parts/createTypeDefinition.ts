import { TypeDefinition } from "../../../../types";
import { ExportModelContext } from "../../exportModelContext.type";
import { getImportedTypes, getMonotypeId } from "../importedTypes/getImportedTypes";
import { mapWith } from "@vlr/fp-conversion";

export const createTypeDefinitions = mapWith(createTypeDefinition);

export function createTypeDefinition(type: string, context: ExportModelContext): TypeDefinition {
  if (type.startsWith("{") || type.endsWith("}")) {
    return { typeName: type, importedType: null, importedTypes: [] };
  }

  return {
    typeName: type,
    importedType: getMonotypeId(type, context),
    importedTypes: getImportedTypes(type, context)
  };
}

