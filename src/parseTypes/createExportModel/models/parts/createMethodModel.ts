import { mapWith } from "@vlr/fp-conversion";
import { MethodModel } from "../../../../types";
import { ParsedFunction } from "../../../parseFile/types";
import { ExportModelContext } from "../../exportModelContext.type";
import { createParameterModel } from "./createParameterModel";
import { createTypeDefinition } from "./createTypeDefinition";

export function createMethodModel(parsed: ParsedFunction, context: ExportModelContext): MethodModel {
  return {
    name: parsed.name,
    comment: parsed.comment,
    parameters: mapWith(createParameterModel)(parsed.parameters, context),
    returnType: createTypeDefinition(parsed.returnType, context)
  };
}
