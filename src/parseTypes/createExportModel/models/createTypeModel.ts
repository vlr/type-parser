import { TypeModel } from "../../../types";
import { ParsedType } from "../../parseFile/types";
import { createTypeId } from "./importedTypes/createTypeId";
import { ExportModelContext } from "../exportModelContext.type";

export function createTypeModel(type: ParsedType, context: ExportModelContext): TypeModel {
  return {
    comment: type.comment,
    id: createTypeId(type.name, context)
  };
}
