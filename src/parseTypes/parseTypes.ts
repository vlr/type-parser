import { TypesModel } from "../types";
import { createExportModel } from "./createExportModel/createExportModel";
import { ParserOptions } from "./parserOptions.type";
import { parseFile } from "./parseFile/parseFile";
import { FileToParse } from "./fileToParse.type";
import { mapWith } from "@vlr/fp-conversion";

const defaultOptions: ParserOptions = { aliasMap: {} };
export function parseTypes<T extends FileToParse>(file: T, options: ParserOptions): TypesModel<T> {
  options = { ...defaultOptions, ...options };
  const parsedModel = parseFile(file.contents);
  return createExportModel(file, parsedModel, options);
}

export const parseTypesInFiles = mapWith(parseTypes);
