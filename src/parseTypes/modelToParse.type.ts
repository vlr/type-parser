import { FileToParse } from "./fileToParse.type";
import { ParserOptions } from "./parserOptions.type";

export interface ModelToParse<T extends FileToParse> {
  file: T;
  options: ParserOptions;
}
