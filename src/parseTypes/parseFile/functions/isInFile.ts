import { BaseState } from "../baseState.type";

export function isInFile(state: BaseState, index: number = state.current): boolean {
  return index < state.end;
}
