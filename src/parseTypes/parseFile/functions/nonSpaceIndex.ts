import { isEmpty, isSpace } from "./isSpace";
import { nextIndex } from "./nextIndex";
import { BaseState } from "../baseState.type";

export function nonSpaceIndex(state: BaseState, index: number = state.current, end: number = state.end): number {
  return nextIndex(state, t => !isSpace(t), index, end);
}

export function emptyIndex(state: BaseState, index: number = state.current, end: number = state.end): number {
  return nextIndex(state, t => isEmpty(t), index, end);
}

export function nonEmptyIndex(state: BaseState, index: number = state.current, end: number = state.end): number {
  return nextIndex(state, t => !isEmpty(t), index, end);
}
