import { BaseState } from "../baseState.type";

export function token(state: BaseState, index: number = state.current): string {
  return state.tokens[index];
}
