import { BaseState } from "../baseState.type";

export function endParsingStage<T extends BaseState>(state: T): T {
  return {
    ...state,
    end: state.current
  };
}
