export function isNameCharacter(token: string): boolean {
  return !!token.match(/^[\$_A-Za-z\d]*$/);
}

export function isNameOrSpaceCharacter(token: string): boolean {
  return !!token.match(/^[\$_A-Za-z\d\s]*$/);
}
