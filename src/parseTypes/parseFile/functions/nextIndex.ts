import { token } from "./currentToken";
import { BaseState } from "../baseState.type";

export function nextIndex(state: BaseState, predicate: (token: string) => boolean, index: number = state.current, end: number = state.end): number {
  while (index < end && !predicate(token(state, index))) {
    index++;
  }
  return index;
}

export function tokenIndex(state: BaseState, token: string, index: number = state.current, end: number = state.end): number {
  return nextIndex(state, t => t === token, index, end);
}
