import { BaseState } from "../baseState.type";
import { nextIndex } from "./nextIndex";

export function isLineFeed(token: string): boolean {
  return token === "\r" || token === "\n" || token === "\r\n";
}

export function lineFeedIndex(state: BaseState, index: number = state.current, end: number = state.end): number {
  return nextIndex(state, isLineFeed, index, end);
}
