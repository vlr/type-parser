import { BaseState } from "../baseState.type";

export function getString(state: BaseState, start: number, end: number): string {
  return state.tokens.slice(start, end).join("");
}

export function getTrimmedString(state: BaseState, start: number, end: number): string {
  return getString(state, start, end).trim();
}
