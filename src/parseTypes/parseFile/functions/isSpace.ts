import { isLineFeed } from "./isLineFeed";

export function isSpace(token: string): boolean {
  return token === " " || token === "\t";
}

export function isEmpty(token: string): boolean {
  return isSpace(token) || isLineFeed(token);
}
