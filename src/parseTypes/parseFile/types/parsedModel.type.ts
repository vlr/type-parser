import { ParsedClass } from "./parsedClass.type";
import { ParsedEnum } from "./parsedEnum.type";
import { ParsedFunction } from "./parsedFunction.type";
import { ParsedImport } from "./parsedImport.type";
import { ParsedInterface } from "./parsedInterface.type";
import { ParsedType } from "./parsedType.type";

export interface ParsedModel {
  imports: ParsedImport[];
  constants: ParsedType[];
  types: ParsedType[];
  enums: ParsedEnum[];
  interfaces: ParsedInterface[];
  classes: ParsedClass[];
  functions: ParsedFunction[];
}
