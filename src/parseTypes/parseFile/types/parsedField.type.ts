export interface ParsedField {
  comment: string;
  fieldName: string;
  typeName: string;
}
