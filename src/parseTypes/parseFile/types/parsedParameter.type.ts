export interface ParsedParameter {
  parameterName: string;
  typeName: string;
}
