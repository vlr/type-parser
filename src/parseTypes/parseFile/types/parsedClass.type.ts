import { ParsedField } from "./parsedField.type";
import { ParsedFunction } from "./parsedFunction.type";
import { ParsedConstructor } from "./parsedConstructor";

export interface ParsedClass {
  comment: string;
  name: string;
  extends: string[];
  implements: string[];
  constructor: ParsedConstructor;
  fields: ParsedField[];
  methods: ParsedFunction[];
}
