export interface ParsedImport {
  type: string;
  alias: string;
  importPath: string;
}
