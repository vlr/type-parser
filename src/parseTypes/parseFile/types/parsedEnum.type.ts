import { ParsedType } from "./parsedType.type";

export interface ParsedEnum extends ParsedType {
  values: ParsedEnumValue[];
}

export interface ParsedEnumValue {
  valueName: string;
  comment: string;
}
