import { ParsedParameter } from "./parsedParameter.type";
import { ParsedType } from "./parsedType.type";

export interface ParsedFunction extends ParsedType {
  returnType: string;
  parameters: ParsedParameter[];
}
