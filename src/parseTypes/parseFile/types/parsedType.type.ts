export interface ParsedType {
  name: string;
  comment: string;
}
