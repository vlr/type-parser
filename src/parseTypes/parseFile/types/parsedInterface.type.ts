import { ParsedField } from "./parsedField.type";
import { ParsedFunction } from "./parsedFunction.type";
import { ParsedType } from "./parsedType.type";

export interface ParsedInterface extends ParsedType {
  extends: string[];
  fields: ParsedField[];
  methods: ParsedFunction[];
}
