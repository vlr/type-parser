import { ParserState } from "./parserState.type";
import { tokenize } from "./tokenize";
import { ParsedModel } from "./types";
import { processToken } from "./processToken/processToken";
import { isInFile } from "./functions/isInFile";

export function parseFile(source: string): ParsedModel {
  const model: ParsedModel = {
    imports: [],
    enums: [],
    classes: [],
    interfaces: [],
    functions: [],
    types: [],
    constants: []
  };
  const tokens = tokenize(source);
  let state: ParserState = {
    model,
    tokens,
    current: 0,
    end: tokens.length,
    lastComment: null
  };
  while (isInFile(state)) {
    state = processToken(state);
  }

  return state.model;
}
