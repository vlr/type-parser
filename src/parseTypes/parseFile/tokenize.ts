import { tokenizePlain } from "@vlr/tokenize";
import { tokens } from "./tokens";

export function tokenize(content: string): string[] {
  return tokenizePlain(content, tokens);
}
