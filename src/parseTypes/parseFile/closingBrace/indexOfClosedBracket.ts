import { last } from "@vlr/array-tools";
import { isClosing } from "./isClosing";

export function indexOfClosedBracket(token: string, stack: string[]): number {
  return isClosing(token, last(stack))
    ? stack.length - 1
    : -1;
}
