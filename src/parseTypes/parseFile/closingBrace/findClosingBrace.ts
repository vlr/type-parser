import { BaseState } from "../baseState.type";
import { moveToIndex } from "../functions/moveToIndex";
import { token } from "../functions/currentToken";
import { isOpening } from "./isOpening";
import { last } from "@vlr/array-tools";
import { indexOfClosedBracket } from "./indexOfClosedBracket";

export function closeBracket<T extends BaseState>(state: T): T {
  const closing = findClosingBracket(state);
  return moveToIndex(state, closing + 1);
}

export function findClosingBracket(state: BaseState, index: number = state.current, end: number = state.end): number {
  let stack: string[] = [token(state, index)];

  index++;
  while (index < end) {
    const tok = token(state, index);
    if (isOpening(tok, last(stack))) {
      stack = [...stack, tok];
    } else {
      const closed = indexOfClosedBracket(tok, stack);
      if (closed === 0) { return index; }
      if (closed > 0) {
        stack = stack.slice(0, closed);
      }
    }

    index++;
  }
  return index;
}
