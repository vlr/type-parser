
const allOpenings = new Set(["{", "[", "${", "<", "(", "\"", "'", "`", "//", "/*"]);
const emptySet = new Set();
const openings = {
  "'": emptySet,
  "\"": emptySet,
  "//": emptySet,
  "/*": emptySet,
  "`": new Set(["${"])
};

export function isOpening(token: string, lastOpening: string = null): boolean {
  const set: Set<string> = openings[lastOpening] || allOpenings;
  return set.has(token);
}
