import { ParserState } from "../../parserState.type";
import { nextIndex } from "../../functions/nextIndex";
import { isInFile } from "../../functions/isInFile";
import { token } from "../../functions/currentToken";
import { moveToIndex } from "../../functions/moveToIndex";

export function skipString(state: ParserState, close: string): ParserState {
  const apostropheIndex = (index: number) => nextIndex(state, t => t === close, index);
  let next = apostropheIndex(state.current + 1);
  while (isInFile(state, next) && token(state, next - 1) === "\\") {
    next = apostropheIndex(next + 1);
  }
  return moveToIndex(state, next + 1);
}
