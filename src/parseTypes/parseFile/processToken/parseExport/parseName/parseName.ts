import { BaseState } from "../../../baseState.type";
import { findClosingBracket } from "../../../closingBrace/findClosingBrace";
import { token } from "../../../functions/currentToken";
import { getTrimmedString } from "../../../functions/getString";
import { isNameCharacter } from "../../../functions/isNameCharacter";
import { nextIndex } from "../../../functions/nextIndex";
import { nonSpaceIndex as nonEmptyIndex } from "../../../functions/nonSpaceIndex";
import { NameState } from "./nameState.type";

export function parseTypeName(state: BaseState, start: number = state.current, end: number = state.end): NameState {
  let current = start = nonEmptyIndex(state, start);
  while (current < end) {
    const t = token(state, current);
    if (t === "<") {
      current = findClosingBracket(state, current) + 1;
      break;
    }

    if (!isNameCharacter(t)) {
      break;
    }

    current++;
  }

  return {
    name: getTrimmedString(state, start, current),
    next: current
  };
}

export function parseName(state: BaseState, start: number = state.current, end: number = state.end): NameState {
  start = nonEmptyIndex(state, start);
  const nameEnd = nextIndex(state, tok => !isNameCharacter(tok), start, end);
  return {
    name: getTrimmedString(state, start, nameEnd),
    next: nameEnd
  };
}
