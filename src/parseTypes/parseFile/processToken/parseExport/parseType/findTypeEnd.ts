import { BaseState } from "../../../baseState.type";
import { token } from "../../../functions/currentToken";
import { isLineFeed } from "../../../functions/isLineFeed";
import { isOpening } from "../../../closingBrace/isOpening";
import { findClosingBracket } from "../../../closingBrace/findClosingBrace";
import { isInFile } from "../../../functions/isInFile";

export function findTypeEnd(state: BaseState, current: number = state.current): number {
  while (isInFile(state, current)) {
    const tok = token(state, current);
    if (tok === ";" || isLineFeed(tok)) {
      return current + 1;
    }
    if (isOpening(tok)) {
      current = findClosingBracket(state, current);
    }
    current++;
  }
  return current;
}
