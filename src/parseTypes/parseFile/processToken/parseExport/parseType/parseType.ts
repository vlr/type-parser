import { ParserState } from "../../../parserState.type";
import { parseTypeName } from "../parseName/parseName";
import { findTypeEnd } from "./findTypeEnd";
import { ParsedType } from "../../../types";

export function parseType(state: ParserState): ParserState {
  const nameState = parseTypeName(state, state.current + 1);
  const end = findTypeEnd(state, nameState.next);
  const type: ParsedType = {
    name: nameState.name,
    comment: state.lastComment
  };

  return {
    ...state,
    model: {
      ...state.model,
      types: [...state.model.types, type]
    },
    current: end
  };
}

export function parseConstant(state: ParserState): ParserState {
  const nameState = parseTypeName(state, state.current + 1);
  const end = findTypeEnd(state, nameState.next);
  const type: ParsedType = {
    name: nameState.name,
    comment: state.lastComment
  };

  return {
    ...state,
    model: {
      ...state.model,
      constants: [...state.model.constants, type]
    },
    current: end
  };
}
