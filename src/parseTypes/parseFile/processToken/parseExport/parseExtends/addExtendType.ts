import { getTrimmedString } from "../../../functions/getString";
import { ExtendsState, ExtendsStateId } from "./extendsState.type";
import { moveTypeStartToNext } from "./moveTypeStartToNext";

export function addExtendType(state: ExtendsState): ExtendsState {
  const type = getTrimmedString(state, state.typeStart, state.current);
  if (type.length === 0) { return moveTypeStartToNext(state); }
  switch (state.stateId) {
    case ExtendsStateId.extends:
      return addToExtendTypes(state, type);
    case ExtendsStateId.implements:
      return addToImplementTypes(state, type);
    default:
      moveTypeStartToNext(state);
  }
}

function addToExtendTypes(state: ExtendsState, type: string): ExtendsState {
  return moveTypeStartToNext({ ...state, extendsTypes: [...state.extendsTypes, type] });
}

function addToImplementTypes(state: ExtendsState, type: string): ExtendsState {
  return moveTypeStartToNext({ ...state, implementsTypes: [...state.implementsTypes, type] });
}
