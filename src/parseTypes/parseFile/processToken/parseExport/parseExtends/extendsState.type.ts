import { BaseState } from "../../../baseState.type";

export enum ExtendsStateId {
  none = "idle",
  extends = "extends",
  implements = "implements"
}

export interface ExtendsState extends BaseState {
  extendsTypes: string[];
  implementsTypes: string[];
  stateId: ExtendsStateId;
  typeStart: number;
}
