import { ExtendsState } from "./extendsState.type";
export function moveTypeStartToNext(state: ExtendsState): ExtendsState {
  const current = state.current + 1;
  return {
    ...state,
    current,
    typeStart: current
  };
}
