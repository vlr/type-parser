import { ExtendsState, ExtendsStateId } from "./extendsState.type";
import { token } from "../../../functions/currentToken";
import { keywords } from "../../../tokens";
import { moveNext } from "../../../functions/moveToIndex";
import { closeBracket } from "../../../closingBrace/findClosingBrace";
import { endParsingStage } from "../../../functions/endParsingStage";
import { addExtendType } from "./addExtendType";

export function parseExtendsToken(state: ExtendsState): ExtendsState {
  switch (token(state)) {
    case keywords.extends:
      return setStateId(state, ExtendsStateId.extends);
    case keywords.implements:
      return setStateId(state, ExtendsStateId.implements);
    case ",":
      return addExtendType(state);
    case "<":
    case "[":
      return closeBracket(state);
    case "{":
      return endParsingStage(addExtendType(state));
    default:
      return moveNext(state);
  }
}

function setStateId(state: ExtendsState, stateId: ExtendsStateId): ExtendsState {
  return { ...addExtendType(state), stateId: stateId };
}
