import { ParserState } from "../../../parserState.type";
import { parseTypeName } from "../parseName/parseName";
import { parseExtends } from "../parseExtends/parseExtends";
import { baseState } from "../../../baseState.type";
import { ExtendsState } from "../parseExtends/extendsState.type";
import { ParsedInterface } from "../../../types";
import { isInFile } from "../../../functions/isInFile";
import { parseInterfaceToken } from "./parseInterfaceToken";
import { InterfaceState } from "./interfaceState.type";

export function parseInterface(state: ParserState): ParserState {
  const nameState = parseTypeName(state, state.current + 1);
  const extendsState = parseExtends(state, nameState.next);
  let interfaceState: InterfaceState = {
    ...baseState(state),
    lastComment: null,
    fields: [],
    methods: [],
    current: extendsState.current
  };

  while (isInFile(interfaceState)) {
    interfaceState = parseInterfaceToken(interfaceState);
  }

  return addInterface(state, nameState.name, extendsState, interfaceState);
}

function addInterface(state: ParserState, name: string, extendsState: ExtendsState, interfaceState: InterfaceState): ParserState {
  const iFace: ParsedInterface = {
    comment: state.lastComment,
    name,
    extends: extendsState.extendsTypes,
    fields: interfaceState.fields,
    methods: interfaceState.methods
  };

  return {
    ...state,
    current: interfaceState.current + 1,
    model: {
      ...state.model,
      interfaces: [...state.model.interfaces, iFace]
    }
  };
}
