import { globalConstants } from "../../../../../../globalConstants";
import { ParsedFunction, ParsedParameter } from "../../../../types";
import { InterfaceState } from "../interfaceState.type";
import { checkStageEnd } from "./findFieldEnd";

export function addMethodToState<T extends InterfaceState>(state: T, name: string,
  parameters: ParsedParameter[], returnType: string, current: number): T {

  if (returnType === "") { returnType = globalConstants.any; }
  const method: ParsedFunction = {
    name,
    parameters,
    returnType,
    comment: state.lastComment
  };
  return checkStageEnd({
    ...state,
    methods: [...state.methods, method],
    current
  });
}
