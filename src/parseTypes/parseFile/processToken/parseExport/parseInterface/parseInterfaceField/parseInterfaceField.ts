import { globalConstants } from "../../../../../../globalConstants";
import { token } from "../../../../functions/currentToken";
import { nonSpaceIndex } from "../../../../functions/nonSpaceIndex";
import { parseName } from "../../parseName/parseName";
import { InterfaceState } from "../interfaceState.type";
import { addFieldToState } from "./addFieldToState";
import { endField, isFieldEnd } from "./findFieldEnd";
import { parseFieldType } from "./parseFieldType";

export function parseInterfaceField(state: InterfaceState): InterfaceState {
  const nameState = parseName(state, state.current);
  const afterName = nonSpaceIndex(state, nameState.next);
  const tok = token(state, afterName);
  if (isFieldEnd(tok)) {
    return addFieldToState(state, nameState.name, globalConstants.any, afterName);
  }

  if (tok === ":") {
    return parseFieldType(state, nameState.name, afterName + 1);
  }

  return endField(state, afterName);
}
