import { InterfaceState } from "../interfaceState.type";
import { endField, findFieldEnd } from "./findFieldEnd";
import { parseParameters } from "../../parseParameters/parseParameters";
import { token } from "../../../../functions/currentToken";
import { nonSpaceIndex } from "../../../../functions/nonSpaceIndex";
import { ParsedParameter } from "../../../../types";
import { getTrimmedString } from "../../../../functions/getString";
import { addMethodToState } from "./addMethodToState";

export function parseInterfaceMethod(state: InterfaceState, name: string, paramsStart: number): InterfaceState {
  const paramState = parseParameters(state, paramsStart);
  const nonSpace = nonSpaceIndex(state, paramState.next);
  const tok = token(state, nonSpace);
  if (tok === "=>") {
    return parseMethodReturnType(state, name, paramState.parameters, nonSpace + 1);
  }

  return endField(state, nonSpace);
}

function parseMethodReturnType(state: InterfaceState, name: string, parameters: ParsedParameter[], returnTypeStart: number): InterfaceState {
  const end = findFieldEnd(state, returnTypeStart);
  const returnType = getTrimmedString(state, returnTypeStart, end);
  return addMethodToState(state, name, parameters, returnType, end);
}
