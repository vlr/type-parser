import { InterfaceState } from "../interfaceState.type";
import { nonSpaceIndex } from "../../../../functions/nonSpaceIndex";
import { token } from "../../../../functions/currentToken";
import { isFieldEnd, findFieldEnd } from "./findFieldEnd";
import { addFieldToState } from "./addFieldToState";
import { globalConstants } from "../../../../../../globalConstants";
import { getTrimmedString } from "../../../../functions/getString";
import { parseInterfaceMethod } from "./parseInterfaceMethod";

export function parseFieldType(state: InterfaceState, name: string, afterColon: number): InterfaceState {
  const typeStart = nonSpaceIndex(state, afterColon);
  const tok = token(state, typeStart);
  if (isFieldEnd(tok)) {
    return addFieldToState(state, name, globalConstants.any, typeStart);
  }
  if (tok === "(") {
    return parseInterfaceMethod(state, name, typeStart + 1);
  }
  const end = findFieldEnd(state, typeStart);
  const typeName = getTrimmedString(state, typeStart, end);
  return addFieldToState(state, name, typeName, end);
}
