import { ParsedField } from "../../../../types";
import { InterfaceState } from "../interfaceState.type";
import { checkStageEnd } from "./findFieldEnd";

export function addFieldToState<T extends InterfaceState>(state: T, fieldName: string, typeName: string, current: number): T {
  const field: ParsedField = {
    fieldName,
    typeName,
    comment: state.lastComment
  };
  return checkStageEnd({
    ...state,
    fields: [...state.fields, field],
    current
  });
}
