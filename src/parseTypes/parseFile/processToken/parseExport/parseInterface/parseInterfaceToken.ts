import { token } from "../../../functions/currentToken";
import { endParsingStage } from "../../../functions/endParsingStage";
import { isEmpty } from "../../../functions/isSpace";
import { moveNext } from "../../../functions/moveToIndex";
import { removeLastComment } from "../../comments/removeLastComment";
import { skipMultiLineComment } from "../../comments/skipMultilineComments";
import { skipOneLineComment } from "../../comments/skipOneLineComment";
import { InterfaceState } from "./interfaceState.type";
import { parseInterfaceField } from "./parseInterfaceField/parseInterfaceField";

export function parseInterfaceToken(state: InterfaceState): InterfaceState {
  const tok = token(state);
  if (isEmpty(tok)) {
    return moveNext(state);
  }

  switch (tok) {
    case "//":
      return skipOneLineComment(state);
    case "/*":
      return skipMultiLineComment(state);
    case "}":
      return endParsingStage(state);
    default:

      return removeLastComment(parseInterfaceField(state));
  }
}
