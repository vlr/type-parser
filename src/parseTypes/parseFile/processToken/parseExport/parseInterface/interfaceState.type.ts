import { ParsedField, ParsedFunction } from "../../../types";
import { StateWithComment } from "../../comments/stateWithComment.type";

export interface InterfaceState extends StateWithComment {
  fields: ParsedField[];
  methods: ParsedFunction[];
}
