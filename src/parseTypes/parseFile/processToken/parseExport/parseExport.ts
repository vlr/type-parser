import { token } from "../../functions/currentToken";
import { moveToIndex } from "../../functions/moveToIndex";
import { nonSpaceIndex } from "../../functions/nonSpaceIndex";
import { ParserState } from "../../parserState.type";
import { types } from "../../tokens";
import { parseFunction } from "./parseFunction/parseFunction";
import { parseInterface } from "./parseInterface/parseInterface";
import { parseType, parseConstant } from "./parseType/parseType";
import { parseEnum } from "./parseEnum/parseEnum";
import { parseClass } from "./parseClass/parseClass";

export function parseExport(state: ParserState): ParserState {
  state = moveToIndex(state, nonSpaceIndex(state, state.current + 1));
  switch (token(state)) {
    case types.function:
      return parseFunction(state);
    case types.interface:
      return parseInterface(state);
    case types.type:
      return parseType(state);
    case types.constant:
      return parseConstant(state);
    case types.enum:
      return parseEnum(state);
    case types.class:
      return parseClass(state);
    default:
      return state;
  }
}
