import { token } from "../../../functions/currentToken";
import { endParsingStage } from "../../../functions/endParsingStage";
import { isEmpty } from "../../../functions/isSpace";
import { moveNext } from "../../../functions/moveToIndex";
import { ParsedEnumValue } from "../../../types";
import { removeLastComment } from "../../comments/removeLastComment";
import { skipMultiLineComment } from "../../comments/skipMultilineComments";
import { skipOneLineComment } from "../../comments/skipOneLineComment";
import { findFieldEnd, checkStageEnd } from "../parseInterface/parseInterfaceField/findFieldEnd";
import { parseName } from "../parseName/parseName";
import { EnumState } from "./enumState.type";

export function parseEnumToken(state: EnumState): EnumState {
  if (isEmpty(token(state))) {
    return moveNext(state);
  }

  switch (token(state)) {
    case "//":
      return skipOneLineComment(state);
    case "/*":
      return skipMultiLineComment(state);
    case "}":
      return endParsingStage(state);
    default:
      return removeLastComment(parseEnumValue(state));
  }
}

function parseEnumValue(state: EnumState): EnumState {
  const nameState = parseName(state, state.current);
  const end = findFieldEnd(state, nameState.next);
  const value: ParsedEnumValue = {
    valueName: nameState.name,
    comment: state.lastComment
  };

  return checkStageEnd({
    ...state,
    values: [...state.values, value],
    current: end
  });
}
