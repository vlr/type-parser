import { baseState } from "../../../baseState.type";
import { isInFile } from "../../../functions/isInFile";
import { ParserState } from "../../../parserState.type";
import { ParsedEnum } from "../../../types";
import { parseTypeName } from "../parseName/parseName";
import { EnumState } from "./enumState.type";
import { parseEnumToken } from "./parseEnumToken";
import { nonSpaceIndex } from "../../../functions/nonSpaceIndex";
import { token } from "../../../functions/currentToken";

export function parseEnum(state: ParserState): ParserState {
  const nameState = parseTypeName(state, state.current + 1);
  const brace = nonSpaceIndex(state, nameState.next);
  if (token(state, brace) !== "{") { return addEnum(state, nameState.name, null); }

  let enumState: EnumState = {
    ...baseState(state),
    current: brace + 1,
    lastComment: null,
    values: []
  };

  while (isInFile(enumState)) {
    enumState = parseEnumToken(enumState);
  }

  return addEnum(state, nameState.name, enumState);
}

function addEnum(state: ParserState, name: string, enumState: EnumState): ParserState {
  const enumType: ParsedEnum = {
    name: name,
    comment: state.lastComment,
    values: enumState && enumState.values || []
  };

  return {
    ...state,
    model: {
      ...state.model,
      enums: [...state.model.enums, enumType]
    },
    current: enumState.end
  };
}
