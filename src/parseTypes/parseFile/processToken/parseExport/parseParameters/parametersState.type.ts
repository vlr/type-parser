import { ParsedParameter } from "../../../types";

export interface ParametersState {
  parameters: ParsedParameter[];
  next: number;
}
