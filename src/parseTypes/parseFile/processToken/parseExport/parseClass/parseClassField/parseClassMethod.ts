import { globalConstants } from "../../../../../../globalConstants";
import { BaseState } from "../../../../baseState.type";
import { findClosingBracket } from "../../../../closingBrace/findClosingBrace";
import { isOpening } from "../../../../closingBrace/isOpening";
import { token } from "../../../../functions/currentToken";
import { getTrimmedString } from "../../../../functions/getString";
import { isInFile } from "../../../../functions/isInFile";
import { nonEmptyIndex } from "../../../../functions/nonSpaceIndex";
import { ParsedParameter } from "../../../../types";
import { addMethodToState } from "../../parseInterface/parseInterfaceField/addMethodToState";
import { endField, findFieldEnd } from "../../parseInterface/parseInterfaceField/findFieldEnd";
import { parseParameters } from "../../parseParameters/parseParameters";
import { ClassState } from "../classState.type";

export function parseClassMethod(state: ClassState, name: string, parametersStart: number): ClassState {
  const paramState = parseParameters(state, parametersStart);
  const nonSpace = nonEmptyIndex(state, paramState.next);
  const tok = token(state, nonSpace);
  if (tok === ":") {
    return parseMethodReturnType(state, name, paramState.parameters, nonSpace + 1);
  }

  if (tok === "{") {
    const closing = findClosingBracket(state, nonSpace);
    return addMethodToState(state, name, paramState.parameters, globalConstants.any, closing);
  }

  if (tok === "}") {
    return addMethodToState(state, name, paramState.parameters, globalConstants.any, nonSpace);
  }

  return endField(state, nonSpace);
}

function parseMethodReturnType(state: ClassState, name: string, params: ParsedParameter[], start: number): ClassState {
  const nonSpace = nonEmptyIndex(state, start);
  switch (token(state, nonSpace)) {
    case "}":
      return addMethodToState(state, name, params, globalConstants.any, nonSpace);
    case "{":
      return parseAnonymousObjectReturnType(state, name, params, nonSpace);
    default:
      return parseRegularReturnType(state, name, params, nonSpace);
  }
}

function parseAnonymousObjectReturnType(state: ClassState, name: string, params: ParsedParameter[], start: number): ClassState {
  const typeEnd = findClosingBracket(state, start) + 1;
  const returnType = getTrimmedString(state, start, typeEnd);
  const end = findFieldEnd(state, typeEnd);
  return addMethodToState(state, name, params, returnType, end);
}

function parseRegularReturnType(state: ClassState, name: string, params: ParsedParameter[], start: number): ClassState {
  const typeEnd = findReturnTypeEnd(state, start);
  const returnType = getTrimmedString(state, start, typeEnd);
  const end = findFieldEnd(state, typeEnd);
  return addMethodToState(state, name, params, returnType, end);
}

function findReturnTypeEnd(state: BaseState, current: number = state.current): number {
  while (isInFile(state, current)) {
    const tok = token(state, current);
    if (tok === "{" || tok === "}") { return current; }
    if (isOpening(tok)) { current = findClosingBracket(state, current); }
    current++;
  }
  return current;
}
