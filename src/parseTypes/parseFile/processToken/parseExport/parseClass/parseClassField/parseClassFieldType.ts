import { BaseState } from "../../../../baseState.type";
import { findClosingBracket } from "../../../../closingBrace/findClosingBrace";
import { isOpening } from "../../../../closingBrace/isOpening";
import { token } from "../../../../functions/currentToken";
import { getTrimmedString } from "../../../../functions/getString";
import { isInFile } from "../../../../functions/isInFile";
import { addFieldToState } from "../../parseInterface/parseInterfaceField/addFieldToState";
import { findFieldEnd } from "../../parseInterface/parseInterfaceField/findFieldEnd";
import { ClassState } from "../classState.type";

export function parseClassFieldType(state: ClassState, name: string, afterColon: number): ClassState {
  const typeEnd = findFieldTypeEnd(state, afterColon);
  const typeName = getTrimmedString(state, afterColon, typeEnd);
  const end = findFieldEnd(state, typeEnd);
  return addFieldToState(state, name, typeName, end);
}

function findFieldTypeEnd(state: BaseState, current: number = state.current): number {
  while (isInFile(state, current)) {
    const tok = token(state, current);
    if (tok === "}" || tok === ";" || tok === "=") { return current; }
    if (isOpening(tok)) {
      current = findClosingBracket(state, current);
    }
    current++;
  }
  return current;
}
