import { token } from "../../../functions/currentToken";
import { endParsingStage } from "../../../functions/endParsingStage";
import { isEmpty } from "../../../functions/isSpace";
import { moveNext } from "../../../functions/moveToIndex";
import { keywords } from "../../../tokens";
import { removeLastComment } from "../../comments/removeLastComment";
import { skipMultiLineComment } from "../../comments/skipMultilineComments";
import { skipOneLineComment } from "../../comments/skipOneLineComment";
import { endField } from "../parseInterface/parseInterfaceField/findFieldEnd";
import { ClassState } from "./classState.type";
import { parseClassField } from "./parseClassField/parseClassField";
import { parseConstructor } from "./parseConstructor";

export function parseClassToken(state: ClassState): ClassState {
  const tok = token(state);
  if (isEmpty(tok)) {
    return moveNext(state);
  }

  switch (token(state)) {
    case "//":
      return skipOneLineComment(state);
    case "/*":
      return skipMultiLineComment(state);
    case "}":
      return endParsingStage(state);
    case keywords.constructor:
      return removeLastComment(parseConstructor(state));
    case keywords.public:
      return removeLastComment(parseClassField(state));
    default:
      return removeLastComment(endField(state));
  }
}





