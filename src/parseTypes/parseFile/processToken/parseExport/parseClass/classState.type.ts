import { ParsedField, ParsedFunction } from "../../../types";
import { ParsedConstructor } from "../../../types/parsedConstructor";
import { StateWithComment } from "../../comments/stateWithComment.type";

export interface ClassState extends StateWithComment {
  constructor: ParsedConstructor;
  fields: ParsedField[];
  methods: ParsedFunction[];
}
