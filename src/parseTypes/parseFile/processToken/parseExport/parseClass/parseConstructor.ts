import { token } from "../../../functions/currentToken";
import { nonSpaceIndex } from "../../../functions/nonSpaceIndex";
import { keywords } from "../../../tokens";
import { ParsedField, ParsedParameter } from "../../../types";
import { findFieldEnd, endField, checkStageEnd } from "../parseInterface/parseInterfaceField/findFieldEnd";
import { parseParameters } from "../parseParameters/parseParameters";
import { ClassState } from "./classState.type";
import { ParsedConstructor } from "../../../types/parsedConstructor";

export function parseConstructor(state: ClassState): ClassState {
  const bracketIndex = nonSpaceIndex(state, state.current + 1);
  if (token(state, bracketIndex) !== "(" || state.constructor != null) {
    return endField(state);
  }

  const paramState = parseParameters(state, bracketIndex + 1);
  const ctor: ParsedConstructor = {
    comment: state.lastComment,
    parameters: removeAccessModifiersOffParameters(paramState.parameters)
  };
  return checkStageEnd<ClassState>({
    ...state,
    constructor: ctor,
    fields: [...state.fields, ...convertPublicParametersToFields(paramState.parameters)],
    current: findFieldEnd(state, paramState.next)
  });
}

function convertPublicParametersToFields(parameters: ParsedParameter[]): ParsedField[] {
  return parameters.filter(x => x.parameterName.startsWith(keywords.public)).map(convertToField);
}

function convertToField(parameter: ParsedParameter): ParsedField {
  const fieldName = parameter.parameterName.substr(keywords.public.length).trim();
  return {
    fieldName,
    typeName: parameter.typeName,
    comment: null
  };
}

function removeAccessModifiersOffParameters(parameters: ParsedParameter[]): ParsedParameter[] {
  return parameters.map(removeAccessModifier);
}

function removeAccessModifier(parameter: ParsedParameter): ParsedParameter {
  const split = splitName(parameter.parameterName);
  if (split.length === 1) { return parameter; }
  const parameterName = split.slice(1).join(" ");
  return {
    parameterName,
    typeName: parameter.typeName
  };
}
function splitName(name: string): string[] {
  return name.split(" ").filter(x => x.length > 0);
}
