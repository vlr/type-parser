import { ParserState } from "../../../parserState.type";
import { ClassState } from "./classState.type";
import { parseTypeName } from "../parseName/parseName";
import { parseExtends } from "../parseExtends/parseExtends";
import { baseState } from "../../../baseState.type";
import { isInFile } from "../../../functions/isInFile";
import { ExtendsState } from "../parseExtends/extendsState.type";
import { ParsedClass } from "../../../types";
import { parseClassToken } from "./parseClassToken";

export function parseClass(state: ParserState): ParserState {
  const nameState = parseTypeName(state, state.current + 1);
  const extendsState = parseExtends(state, nameState.next);
  let classState: ClassState = {
    ...baseState(state),
    lastComment: null,
    constructor: null,
    fields: [],
    methods: [],
    current: extendsState.current
  };

  while (isInFile(classState)) {
    classState = parseClassToken(classState);
  }

  return addClass(state, nameState.name, extendsState, classState);
}

function addClass(state: ParserState, name: string, extendsState: ExtendsState, classState: ClassState): ParserState {
  const cls: ParsedClass = {
    name,
    extends: extendsState.extendsTypes,
    implements: extendsState.implementsTypes,
    comment: state.lastComment,
    constructor: classState.constructor,
    fields: classState.fields,
    methods: classState.methods
  };
  return {
    ...state,
    current: classState.current + 1,
    model: {
      ...state.model,
      classes: [...state.model.classes, cls]
    }
  };
}
