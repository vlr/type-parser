import { findClosingBracket } from "../../../closingBrace/findClosingBrace";
import { isInFile } from "../../../functions/isInFile";
import { tokenIndex } from "../../../functions/nextIndex";
import { ParserState } from "../../../parserState.type";
import { ParsedFunction } from "../../../types";
import { parseTypeName } from "../parseName/parseName";
import { parseParameters } from "../parseParameters/parseParameters";
import { parseReturnType } from "./parseReturnType";

export function parseFunction(state: ParserState): ParserState {
  const nameState = parseTypeName(state, state.current + 1);
  const leftBrace = tokenIndex(state, "(", nameState.next);
  if (!isInFile(state, leftBrace)) { return state; }

  const parametersState = parseParameters(state, leftBrace + 1);
  const afterParams = parametersState.next;
  if (!isInFile(state)) { return state; }

  const curlyBrace = tokenIndex(state, "{", afterParams);
  if (!isInFile(state, curlyBrace)) { return state; }

  const closingBrace = findClosingBracket(state, curlyBrace);
  const name = nameState.name;
  const returnType = parseReturnType(state, afterParams, curlyBrace);
  if (name == null || name === "" || returnType == null) { return state; }

  const newFunc = { name, parameters: parametersState.parameters, returnType, comment: state.lastComment };
  return addFunction(state, newFunc, closingBrace);
}

function addFunction(state: ParserState, func: ParsedFunction, closing: number): ParserState {
  return {
    ...state,
    model: {
      ...state.model,
      functions: [...state.model.functions, func]
    },
    current: closing + 1
  };
}
