import { tokenIndex } from "../../../functions/nextIndex";
import { getTrimmedString } from "../../../functions/getString";
import { BaseState } from "../../../baseState.type";

export function parseReturnType(state: BaseState, start: number, end: number): string {
  const colon = tokenIndex(state, ":", start);
  if (colon >= end) { return null; }
  const returnType = getTrimmedString(state, colon + 1, end);
  return returnType.length === 0 ? null : returnType;
}
