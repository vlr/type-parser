import { closeBracket } from "../closingBrace/findClosingBrace";
import { isOpening } from "../closingBrace/isOpening";
import { token } from "../functions/currentToken";
import { isLineFeed } from "../functions/isLineFeed";
import { moveNext } from "../functions/moveToIndex";
import { ParserState } from "../parserState.type";
import { keywords } from "../tokens";
import { parseExport } from "./parseExport/parseExport";
import { parseImport } from "./parseImport/parseImport";
import { skipMultiLineComment } from "./comments/skipMultilineComments";
import { skipOneLineComment } from "./comments/skipOneLineComment";
import { removeLastComment } from "./comments/removeLastComment";

export function processToken(state: ParserState): ParserState {
  // todo avoid catching exports inside strings and etc.
  switch (token(state)) {
    case "//":
      return skipOneLineComment(state);
    case "/*":
      return skipMultiLineComment(state);
    case keywords.import:
      return removeLastComment(parseImport(state));
    case keywords.export:
      return removeLastComment(parseExport(state));
    case " ":
    case "\t":
      return moveNext(state);
    default:
      return processUnimportantToken(state);
  }
}

function processUnimportantToken(state: ParserState): ParserState {
  if (isLineFeed(token(state))) { return moveNext(state); }
  if (isOpening(token(state))) { return removeLastComment(closeBracket(state)); }
  return removeLastComment(moveNext(state));
}


