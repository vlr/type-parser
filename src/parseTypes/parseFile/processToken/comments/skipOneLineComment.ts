import { getTrimmedString } from "../../functions/getString";
import { lineFeedIndex } from "../../functions/isLineFeed";
import { StateWithComment } from "./stateWithComment.type";

export function skipOneLineComment<T extends StateWithComment>(state: T): T {
  const linefeed = lineFeedIndex(state);
  const comment = getTrimmedString(state, state.current + 1, linefeed);
  return {
    ...state,
    lastComment: comment,
    current: linefeed + 1
  };
}
