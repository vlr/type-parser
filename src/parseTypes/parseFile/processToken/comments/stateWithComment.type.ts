import { BaseState } from "../../baseState.type";

export interface StateWithComment extends BaseState {
  lastComment: string;
}
