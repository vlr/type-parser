import { StateWithComment } from "./stateWithComment.type";

export function removeLastComment<T extends StateWithComment>(state: T): T {
  return { ...state, lastComment: null };
}
