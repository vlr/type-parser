import { getTrimmedString } from "../../functions/getString";
import { tokenIndex } from "../../functions/nextIndex";
import { StateWithComment } from "./stateWithComment.type";

export function skipMultiLineComment<T extends StateWithComment>(state: T): T {
  const closing = tokenIndex(state, "*/");
  const comment = getTrimmedString(state, state.current + 1, closing);
  return {
    ...state,
    lastComment: comment,
    current: closing + 1
  };
}
