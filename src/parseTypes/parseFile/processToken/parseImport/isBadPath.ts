export function isBadPath(path: string): boolean {
  return !path.match(/^@?[^@\s\[\]{}\(\)<>;]+$/);
}
