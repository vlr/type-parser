import { token } from "../../functions/currentToken";

import { tokenIndex, nextIndex } from "../../functions/nextIndex";
import { emptyIndex, nonEmptyIndex } from "../../functions/nonSpaceIndex";
import { ParserState } from "../../parserState.type";
import { keywords } from "../../tokens";
import { getString, getTrimmedString } from "../../functions/getString";
import { isBadPath } from "./isBadPath";
import { moveNext, moveToIndex } from "../../functions/moveToIndex";
import { createImports } from "./createImports";

function isQuote(token: string): boolean {
  return token === "'" || token === "\"";
}

export function parseImport(state: ParserState): ParserState {
  state = moveNext(state);
  const leftBracket = nonEmptyIndex(state);
  const fallback = () => moveToIndex(state, emptyIndex(state, leftBracket));
  if (token(state, leftBracket) !== "{") { return fallback(); }
  const rightBracket = tokenIndex(state, "}");
  const from = nonEmptyIndex(state, rightBracket + 1);
  if (token(state, from) !== keywords.from) { return fallback(); }
  const leftQuote = nonEmptyIndex(state, from + 1);
  if (!isQuote(token(state, leftQuote))) { return fallback(); }
  const rightQuote = nextIndex(state, isQuote, leftQuote + 1);
  const imports = getString(state, leftBracket + 1, rightBracket);
  const importPath = getTrimmedString(state, leftQuote + 1, rightQuote);
  if (isBadPath(importPath)) { return fallback(); }
  return createImports(moveToIndex(state, rightQuote + 1), imports, importPath);
}
