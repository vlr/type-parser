import { ParserState } from "../../parserState.type";
import { ParsedImport } from "../../types";

export function createImports(state: ParserState, imports: string, path: string): ParserState {
  const parsedImports = imports.split(",").map(i => createImport(i, path));
  return {
    ...state,
    model: {
      ...state.model,
      imports: [...state.model.imports, ...parsedImports]
    }
  };
}

function createImport(importText: string, importPath: string): ParsedImport {
  const parts = importText.split(/\s[\s]*/gs).filter(s => s.length);
  if (!parts.length) { return null; }
  const index = parts.length === 3 && parts[1] === "as" ? 2 : 0;
  return {
    type: parts[0],
    alias: parts[index],
    importPath
  };
}
