export interface FileToParse {
  contents: string;
  folder: string;
  name: string;
}
